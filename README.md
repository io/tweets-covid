# Les débuts du COVID sur les réseaux sociaux : collecte et analyse de données Twitter

VF1 2022-09-10

## Présentation du Notebook

Ce notebook présente les étapes de collecte, manipulation et analyse de données Twitter. A partir d'un jeu de données disponible, il analyse les tweets correspondant aux jours avant le confinement français de 2020.

Données utilisées : liste des identifiants de tweets associés au COVID-19 multilangue collectées par une équipe de recherche et rendue disponible sur Zénodo https://zenodo.org/record/3723940

Niveau du Notebook : intermédiaire

Principales notions vues :

- collecte de donnée par une API
- stockage de grandes quantités de données
- filtre et analyse qualitative à partir de tableaux
- analyse de sentiment avec des transformers
- analyse de réseaux et visualisations


# Contenu du notebook

Présentation générale

Éléments du contexte (données et bibliothèques)

Récupération des données

1.1 Récupérer les identifiants des tweets

1.2 Hydratation des tweets

1.3 Récupérer uniquement les tweets en français

Exploration du corpus

2.1 Evolution générale

2.2 Identifier les tweets les plus visibles

2.3 Extraire de l'information d'un tweet : les #hashtags

2.4 La circulation des URLS

Application de détection des sentiments sur les tweets

3.1 Choix des modèles

3.2 Application sur le corpus

3.3 Orientation des différents comptes

Analyse de réseaux

4.1 Groupes de mots clés

4.2 Réseau du partage de lien

4.3 Réseau d'utilisateurs

Conclusion

## Exécution du Notebook

Ce notebook a été conçu pour le noyau Callisto *Deep Learning Python 3.8*.

Un fichier *requirements.txt* contient les bibliothèques nécessaires à l'exécution.

## Crédits

Auteurs
Ce notebook a été réalisé par Émilien Schultz (@EmilienSchultz) et Mathieu Morey.

## Licence

MIT License

Copyright (c) 2022 Huma-Num Lab

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:
The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.
THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.